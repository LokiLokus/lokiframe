using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LaberlChecker.Database;
using LaberlChecker.Service;
using LaberlChecker.Service.Helper;
using LokiFrame;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace LaberlChecker
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            Configuration.GetSetting<string>("Logging");
            services.Configure<string>(Configuration.GetSection("Logging"));
           services.AddDbContext<DatabaseCtx>(opt => opt.UseInMemoryDatabase("asd"));
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "LabelChecker", Version = "v1"});
            });

            
            services.AddLokiFrame(d =>
            {
                d.UseDefaultErrorText = false;
                d.UseExceptionMessage = true;
                d.UseExceptionStackTrace = true;
            });
            StartupHelper.InitAllMapper(services);
            StartupHelper.AddAllServices(services,Configuration);
            services.AddControllers().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });
            services.AddTransient<CustomerService, CustomerService>();
            services.AddTransient<OrderService, OrderService>();
            services.AddTransient<ProductService, ProductService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env,IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "Label API"); });
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
            InitHelper.Init(serviceProvider).Wait();
        }
    }
}