using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LaberlChecker.Database.Model
{
    public class Scan
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string ScanId { get; set; }

        public string Code { get; set; }
        public short Quality { get; set; }
        public int OrderPrio { get; set; }

        [ForeignKey(nameof(Label))]
        public string LabelId { get; set; }
        public Label Label { get; set; }
        public Roll Roll { get; set; }
        public IList<History> Histories { get; set; }
    }
}