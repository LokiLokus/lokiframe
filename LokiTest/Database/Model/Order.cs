using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LaberlChecker.Database.Model
{
    public class Order
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string OrderId { get; set; }

        public string Name { get; set; }
        public OrderState OrderState { get; set; }

        [ForeignKey(nameof(Product))]
        public string ProductId { get; set; }
        
        [ForeignKey(nameof(Customer))]
        public string CustomerId { get; set; }
        public Product Product { get; set; }
        public Customer Customer { get; set; }
        public IList<Loot> Loots { get; set; }
    }

    public enum OrderState
    {
        
    }
}