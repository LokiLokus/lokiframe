using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LaberlChecker.Database.Model
{
    public class Roll
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string RollId { get; set; }

        public string Name { get; set; }
        public Loot Loot { get; set; }
        public IList<Label> Labels { get; set; }
        public IList<Scan> Scans { get; set; }
    }
}