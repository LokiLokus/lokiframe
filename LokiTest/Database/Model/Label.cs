using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LaberlChecker.Database.Model
{
    public class Label
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string LabelId { get; set; }

        public string Code { get; set; }
        public short Quality { get; set; }
        public int OrderPrio { get; set; }
        
        public Roll Roll { get; set; }
        [ForeignKey(nameof(Scan))]
        public string ScanId { get; set; }
        public Scan Scan { get; set; }
    }
}