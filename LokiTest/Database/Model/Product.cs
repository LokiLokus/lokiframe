using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LaberlChecker.Database.Model
{
    public class Product
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string ProductId { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string NumberGenerator { get; set; }
        public IList<Order> Orders { get; set; }
    }
}