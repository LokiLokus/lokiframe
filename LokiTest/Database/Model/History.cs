using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LaberlChecker.Database.Model
{
    public class History
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string HistoryId { get; set; }
        
        public string Code { get; set; }
        public short Quality { get; set; }
        public int OrderPrio { get; set; }

        public Scan Scan { get; set; }
    }
}