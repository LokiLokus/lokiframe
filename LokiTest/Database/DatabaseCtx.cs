using LaberlChecker.Database.Model;
using Microsoft.EntityFrameworkCore;

namespace LaberlChecker.Database
{
    public class DatabaseCtx : DbContext
    {
        
        public DbSet<Customer> Customers { get; set; }
        public DbSet<History> Histories { get; set; }
        public DbSet<Label> Labels { get; set; }
        public DbSet<Loot> Loots { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Roll> Rolls { get; set; }
        public DbSet<Scan> Scans { get; set; }
        
        
        public DatabaseCtx(DbContextOptions<DatabaseCtx> options) : base(options)
        {
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>()
                .HasMany(x => x.Orders)
                .WithOne(d => d.Customer);
            modelBuilder.Entity<Product>()
                .HasMany(x => x.Orders)
                .WithOne(d => d.Product);
            modelBuilder.Entity<Order>()
                .HasMany(x => x.Loots)
                .WithOne(d => d.Order);
            modelBuilder.Entity<Loot>()
                .HasMany(d => d.Rolls)
                .WithOne(d => d.Loot);
            modelBuilder.Entity<Roll>()
                .HasMany(d => d.Labels)
                .WithOne(d => d.Roll);
            modelBuilder.Entity<Label>()
                .HasOne(d => d.Scan)
                .WithOne(d => d.Label);
            modelBuilder.Entity<Roll>()
                .HasMany(d => d.Scans)
                .WithOne(d => d.Roll);
            modelBuilder.Entity<Scan>()
                .HasMany(d => d.Histories)
                .WithOne(d => d.Scan);

            base.OnModelCreating(modelBuilder);
        }
    }
}