using System.Collections.Generic;
using LaberlChecker.ViewModel.Order;

namespace LaberlChecker.ViewModel.Product
{
    public class ProductModel:ProductCreateModel
    {
        public string ProductId { get; set; }

        public IList<OrderModel> Orders { get; set; }
    }
}