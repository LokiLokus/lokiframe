namespace LaberlChecker.ViewModel.Product
{
    public class ProductCreateModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string NumberGenerator { get; set; }
    }
}