using System.Collections.Generic;
using LaberlChecker.ViewModel.Order;

namespace LaberlChecker.ViewModel.Customer
{
    public class CustomerModel: CustomerCreateModel
    {
        public string CustomerId { get; set; }
        public IList<OrderModel> Orders { get; set; }
    }
}