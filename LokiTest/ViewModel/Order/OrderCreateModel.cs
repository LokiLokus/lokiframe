using LaberlChecker.Database.Model;

namespace LaberlChecker.ViewModel.Order
{
    public class OrderCreateModel
    {
        public string Name { get; set; }
        public OrderState OrderState { get; set; }
        public string ProductId { get; set; }
        public string CustomerId { get; set; }
    }
}