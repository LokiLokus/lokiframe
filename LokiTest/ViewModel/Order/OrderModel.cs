using System.Collections.Generic;
using LaberlChecker.ViewModel.Customer;
using LaberlChecker.ViewModel.Loot;
using LaberlChecker.ViewModel.Product;

namespace LaberlChecker.ViewModel.Order
{
    public class OrderModel: OrderCreateModel
    {
        public string OrderId { get; set; }
        public IList<LootModel> Loots { get; set; }
        public CustomerModel Customer { get; set; }
        public ProductModel Product { get; set; }
    }
}