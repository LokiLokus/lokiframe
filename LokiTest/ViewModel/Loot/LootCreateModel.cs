namespace LaberlChecker.ViewModel.Loot
{
    public class LootCreateModel
    {
        public string Name { get; set; }
        public string OrderId { get; set; }
    }
}