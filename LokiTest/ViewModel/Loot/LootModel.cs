using System.Collections.Generic;
using LaberlChecker.Database.Model;
using LaberlChecker.ViewModel.Order;
using LaberlChecker.ViewModel.Roll;

namespace LaberlChecker.ViewModel.Loot
{
    public class LootModel: LootCreateModel
    {
        public string LootId { get; set; }
        public IList<RollModel> Rolls { get; set; }
        public OrderModel Order { get; set; }
    }
}