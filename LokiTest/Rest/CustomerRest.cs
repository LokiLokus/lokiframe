using System.Collections.Generic;
using System.Threading.Tasks;
using LaberlChecker.Service;
using LaberlChecker.ViewModel.Customer;
using LokiFrame.Rest;
using Microsoft.AspNetCore.Mvc;

namespace LaberlChecker.Rest
{
    public class CustomerController: LokiCRU<CustomerService,CustomerCreateModel,CustomerCreateModel,CustomerModel>
    {
        public CustomerController(CustomerService service) : base(service)
        {
        }

        public override async Task<ActionResult<IEnumerable<CustomerModel>>> Get()
        {
            return await CallRest(_service.Get);
        }

        public override async Task<ActionResult<CustomerModel>> Create(CustomerCreateModel model)
        {
            return await CallRest(_service.Create,model);
        }

        public override async Task<ActionResult<CustomerModel>> Update(CustomerCreateModel model, string id)
        {
            return await CallRest(_service.Update, model,id);
        }


    }
}