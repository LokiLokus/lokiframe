using System.Collections.Generic;
using System.Threading.Tasks;
using LaberlChecker.Service;
using LaberlChecker.ViewModel.Order;
using LaberlChecker.ViewModel.Product;
using LokiFrame.Rest;
using Microsoft.AspNetCore.Mvc;

namespace LaberlChecker.Rest
{
    public class OrderController:LokiCRU<OrderService,OrderCreateModel,OrderCreateModel,OrderModel>
    {
        public OrderController(OrderService service) : base(service)
        {
        }

        public override async Task<ActionResult<IEnumerable<OrderModel>>> Get()
        {
            return await CallRest(_service.Get);
        }

        public override async Task<ActionResult<OrderModel>> Create(OrderCreateModel model)
        {
            return await CallRest(_service.Create,model);
        }

        public override async Task<ActionResult<OrderModel>> Update(OrderCreateModel model, string id)
        {
            return await CallRest(_service.Update,model,id);
        }
    }
}