using System.Collections.Generic;
using System.Threading.Tasks;
using LaberlChecker.Service;
using LaberlChecker.ViewModel.Loot;
using LokiFrame.Rest;
using Microsoft.AspNetCore.Mvc;

namespace LaberlChecker.Rest
{
    public class LootController: LokiCRU<LootService,LootCreateModel,LootCreateModel,LootModel>
    {
        public LootController(LootService service) : base(service)
        {
        }

        public override async Task<ActionResult<IEnumerable<LootModel>>> Get()
        {
            return await CallRest(_service.Get);
        }

        public override async Task<ActionResult<LootModel>> Create(LootCreateModel model)
        {
            return await CallRest(_service.Create,model);
        }

        public override async Task<ActionResult<LootModel>> Update(LootCreateModel model, string id)
        {
            return await CallRest(_service.Update,model,id);
        }
    }
}