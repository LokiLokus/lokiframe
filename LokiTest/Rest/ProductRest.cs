using System.Collections.Generic;
using System.Threading.Tasks;
using LaberlChecker.Service;
using LaberlChecker.ViewModel.Customer;
using LaberlChecker.ViewModel.Product;
using LokiFrame.Rest;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Storage;

namespace LaberlChecker.Rest
{
    public class ProductController:LokiCRU<ProductService,ProductCreateModel,ProductCreateModel,ProductModel>
    {
        public ProductController(ProductService service) : base(service)
        {
        }

        public override async Task<ActionResult<IEnumerable<ProductModel>>> Get()
        {
            return await CallRest(_service.Get);
        }

        public override async Task<ActionResult<ProductModel>> Create(ProductCreateModel model)
        {
            return await CallRest(_service.Create,model);
        }

        public override async Task<ActionResult<ProductModel>> Update(ProductCreateModel model, string id)
        {
            return await CallRest(_service.Update,model,id);
        }
    }
}