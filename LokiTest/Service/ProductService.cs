using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using LaberlChecker.Database;
using LaberlChecker.Database.Model;
using LaberlChecker.ViewModel.Customer;
using LaberlChecker.ViewModel.Product;
using LokiFrame;
using LokiFrame.Rest.Interfaces;
using LokiFrame.Service;
using Microsoft.EntityFrameworkCore;

namespace LaberlChecker.Service
{
    public class ProductService:LokiService<DatabaseCtx,IMapper>,ICRU<ProductCreateModel,ProductCreateModel,ProductModel>
    {
        public ProductService(DatabaseCtx databaseCtx, IMapper mapper) : base(databaseCtx, mapper)
        {
        }
        
        public async Task<Result<IEnumerable<ProductModel>>> Get()
        {
            var products = await _databaseCtx.Products.Include(x => x.Orders).ToListAsync();
            var result = _mapper.Map<IEnumerable<ProductModel>>(products);
            return Result.Ok(result);
        }
        public async Task<Result<ProductModel>> Create(ProductCreateModel model)
        {
            if(model == null) throw new ArgumentNullException("Model is null");
            Product product = _mapper.Map<Product>(model);
            await _databaseCtx.Products.AddAsync(product);
            await _databaseCtx.SaveChangesAsync();
            var result = _mapper.Map<ProductModel>(product);
            return Result.Ok(result);
        }

        public async Task<Result<ProductModel>> Update(ProductCreateModel model, string id)
        {
            var product = await _databaseCtx.Customers.SingleOrDefaultAsync(d => d.CustomerId == id);
            if (product == null) return Result.Fail<ProductModel>("CustomerId", "Kunde nicht gefunden");
            _mapper.Map(model, product);
            await _databaseCtx.SaveChangesAsync();
            var result = _mapper.Map<ProductModel>(product);
            return Result.Ok(result);
        }
    }
}