using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using LaberlChecker.Database;
using LaberlChecker.Database.Model;
using LaberlChecker.ViewModel.Loot;
using LokiFrame;
using LokiFrame.Rest.Interfaces;
using LokiFrame.Service;
using Microsoft.EntityFrameworkCore;

namespace LaberlChecker.Service
{
    public class LootService:LokiService<DatabaseCtx,IMapper>,ICRU<LootCreateModel,LootCreateModel,LootModel>
    {
        public LootService(DatabaseCtx databaseCtx, IMapper mapper) : base(databaseCtx, mapper)
        {
        }

        public async Task<Result<IEnumerable<LootModel>>> Get()
        {
            var data = await _databaseCtx.Loots.Include(x => x.Rolls).ToListAsync();
            return Result.Ok(_mapper.Map<IEnumerable<LootModel>>(data));
        }

        public async Task<Result<LootModel>> Create(LootCreateModel model)
        {
            var order = await _databaseCtx.Orders.AnyAsync(d => d.OrderId == model.OrderId);
            if (!order) return Result.Fail<LootModel>("OrderId", "Auftrag wurde nicht gefunden");
            Loot loot = _mapper.Map<Loot>(model);
            await _databaseCtx.Loots.AddAsync(loot);
            await _databaseCtx.SaveChangesAsync();
            return Result.Ok(_mapper.Map<LootModel>(loot));
        }

        public async Task<Result<LootModel>> Update(LootCreateModel model, string id)
        {
            var order = await _databaseCtx.Orders.AnyAsync(d => d.OrderId == model.OrderId);
            var loot = await _databaseCtx.Loots.SingleOrDefaultAsync(x => x.LootId == id);
            if (loot == null) return Result.Fail<LootModel>("LootId", "Loot wurde nicht gefunden");
            if (!order) return Result.Fail<LootModel>("OrderId", "Auftrag wurde nicht gefunden");
            _mapper.Map(model,loot);
            await _databaseCtx.Loots.AddAsync(loot);
            await _databaseCtx.SaveChangesAsync();
            return Result.Ok(_mapper.Map<LootModel>(loot));
        }
    }
}
