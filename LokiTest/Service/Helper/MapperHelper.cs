using AutoMapper;
using LaberlChecker.Database.Model;
using LaberlChecker.ViewModel.Customer;
using LaberlChecker.ViewModel.Loot;
using LaberlChecker.ViewModel.Order;
using LaberlChecker.ViewModel.Product;
using LokiFrame;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace LaberlChecker.Service.Helper
{
    public class MapperHelper: IComponentDefinition
    {
        public MapperHelper()
        {
            CreateMap<CustomerModel, Customer>().ReverseMap();
            CreateMap<CustomerCreateModel, Customer>().ReverseMap();
            CreateMap<ProductCreateModel, Product>().ReverseMap();
            CreateMap<Product, ProductModel>().ReverseMap();
            CreateMap<Order, OrderModel>().ReverseMap();
            CreateMap<Order, OrderCreateModel>().ReverseMap();
            CreateMap<Loot, LootModel>().ReverseMap();
            CreateMap<Loot, LootCreateModel>().ReverseMap();
        }

        public override void AddServices(IServiceCollection services, IConfiguration configuration)
        {
        }
    }
}