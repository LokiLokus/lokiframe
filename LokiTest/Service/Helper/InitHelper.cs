using System;
using System.Linq;
using System.Threading.Tasks;
using LaberlChecker.ViewModel.Customer;
using LaberlChecker.ViewModel.Order;
using LaberlChecker.ViewModel.Product;
using Microsoft.Extensions.DependencyInjection;

namespace LaberlChecker.Service.Helper
{
    public class InitHelper
    {
        public static async Task Init(IServiceProvider provider)
        {
            var customerService = provider.GetService<CustomerService>();
            var productService = provider.GetService<ProductService>();
            var orderService = provider.GetService<OrderService>();
            await InitCustomer(customerService);
            await InitProduct(productService);
            await InitOrders(orderService, productService, customerService);

        }

        private static async Task InitOrders(OrderService service, ProductService productService, CustomerService customerService)
        {
            await service.Create(new OrderCreateModel()
            {
                Name = "asdasd",
                CustomerId = customerService.Get().Result.SuccessResult.First().CustomerId,
                ProductId = productService.Get().Result.SuccessResult.First().ProductId
            });
        }

        private static async Task InitProduct(ProductService getService)
        {
            await getService.Create(new ProductCreateModel()
            {
                Name = "Test",
                Description = "asdas"
            });
        }

        private static async Task InitCustomer(CustomerService service)
        {
            await service.Create(new CustomerCreateModel()
            {
                Name = "Test"
            });
            await service.Create(new CustomerCreateModel()
            {
                Name = "Test2"
            });
        }
    }
}