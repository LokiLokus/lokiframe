using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using LaberlChecker.Database;
using LaberlChecker.Database.Model;
using LaberlChecker.ViewModel.Order;
using LokiFrame;
using LokiFrame.Rest.Interfaces;
using LokiFrame.Service;
using Microsoft.EntityFrameworkCore;

namespace LaberlChecker.Service
{
    public class OrderService:LokiService<DatabaseCtx,IMapper>,ICRU<OrderCreateModel,OrderCreateModel,OrderModel>
    {
        public OrderService(DatabaseCtx databaseCtx, IMapper mapper) : base(databaseCtx, mapper)
        {
        }

        public async Task<Result<IEnumerable<OrderModel>>> Get()
        {
            var orders = await _databaseCtx.Orders.Include(x => x.Loots).Include(x => x.Customer).Include(x => x.Product).ToListAsync();
            var result = _mapper.Map<IEnumerable<OrderModel>>(orders);
            return Result.Ok(result);
        }

        public async Task<Result<OrderModel>> Create(OrderCreateModel model)
        {
            var product = await _databaseCtx.Products.SingleOrDefaultAsync(x => x.ProductId == model.ProductId);
            var customer = await _databaseCtx.Customers.SingleOrDefaultAsync(x => x.CustomerId == model.CustomerId);
            if (product == null) return Result.Fail<OrderModel>("ProductId", "Das Produkt ist nicht bekannt");
            if (customer == null) return Result.Fail<OrderModel>("CustomerId", "Der Kunde ist nicht bekannt");
            Order order = _mapper.Map<Order>(model);
            await _databaseCtx.Orders.AddAsync(order);
            await _databaseCtx.SaveChangesAsync();
            var result = _mapper.Map<OrderModel>(order);
            return Result.Ok(result);
        }

        public async Task<Result<OrderModel>> Update(OrderCreateModel model, string id)
        {
            var product = await _databaseCtx.Products.SingleOrDefaultAsync(x => x.ProductId == model.ProductId);
            var customer = await _databaseCtx.Customers.SingleOrDefaultAsync(x => x.CustomerId == model.CustomerId);
            var order = await _databaseCtx.Orders.SingleOrDefaultAsync(d => d.OrderId == id);
            if (product == null) return Result.Fail<OrderModel>("ProductId", "Das Produkt ist nicht bekannt");
            if (customer == null) return Result.Fail<OrderModel>("CustomerId", "Der Kunde ist nicht bekannt");
            if(order == null) return Result.Fail<OrderModel>("OrderId","Der Auftrag ist nicht bekannt");
            _mapper.Map(model, order);
            await _databaseCtx.SaveChangesAsync();
            var result = _mapper.Map<OrderModel>(order);
            return Result.Ok(result);
        }
    }
}