using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using LaberlChecker.Database;
using LaberlChecker.Database.Model;
using LaberlChecker.ViewModel.Customer;
using LokiFrame;
using LokiFrame.Rest.Interfaces;
using LokiFrame.Service;
using Microsoft.EntityFrameworkCore;

namespace LaberlChecker.Service
{
    public class CustomerService:LokiService<DatabaseCtx,IMapper>, ICRU<CustomerCreateModel,CustomerCreateModel,CustomerModel>

    {
        public CustomerService(DatabaseCtx databaseCtx,IMapper mapper) : base(databaseCtx,mapper)
        {
        }

        public async Task<Result<IEnumerable<CustomerModel>>> Get()
        {
            var customers = await _databaseCtx.Customers.Include(x => x.Orders).ToListAsync();
            var result = _mapper.Map<IEnumerable<CustomerModel>>(customers);
            return Result.Ok(result);
        }
        
        public async Task<Result<CustomerModel>> Create(CustomerCreateModel model)
        {
            if(model == null) throw new ArgumentNullException("Model is null");
            Customer customer = _mapper.Map<Customer>(model);
            await _databaseCtx.Customers.AddAsync(customer);
            await _databaseCtx.SaveChangesAsync();
            var result = _mapper.Map<CustomerModel>(customer);
            return Result.Ok(result);
        }

        public async Task<Result<CustomerModel>> Update(CustomerCreateModel model,string id)
        {
            var customer = await _databaseCtx.Customers.SingleOrDefaultAsync(d => d.CustomerId == id);
            if (customer == null) return Result.Fail<CustomerModel>("CustomerId", "Kunde nicht gefunden");
            _mapper.Map(model, customer);
            await _databaseCtx.SaveChangesAsync();
            var result = _mapper.Map<CustomerModel>(customer);
            return Result.Ok(result);
        }
    }
}