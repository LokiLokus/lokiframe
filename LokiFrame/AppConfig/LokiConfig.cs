using System;

namespace LokiFrame.AppConfig
{
    public class LokiConfig
    {
        internal static LokiConfig _config = new LokiConfig();
        public bool RethrowException { get; set; } = false;
        public bool UseDefaultErrorText { get; set; } = true;
        public bool UseExceptionStackTrace { get; set; } = false;
        public bool UseExceptionMessage { get; set; } = false;
        public bool PrintException { get; set; } = true;
        public string DefaultErrorText { get; set; } = "Error occured";
        public string DefaultErrorCode { get; set; } = "General";

        public void ValidateConfig()
        {
            if(UseDefaultErrorText && UseExceptionMessage)
                throw new Exception("Can not use Default Error Text and Exception Message");
            if(!RethrowException && !UseDefaultErrorText && ! UseExceptionMessage)
                throw new Exception("Must print something on Exception (Default or Exception Message)");
        }
    }
}