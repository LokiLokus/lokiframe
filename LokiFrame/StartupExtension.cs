using System;
using AutoMapper;
using LokiFrame.AppConfig;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace LokiFrame
{
    public static class StartupExtension
    {
        public static IServiceCollection AddLokiFrame(this IServiceCollection services,Action<LokiConfig> configAction)
        {
            LokiConfig config = new LokiConfig();
            configAction.Invoke(config);
            return services.AddLokiFrame(config);
        }
        public static IServiceCollection AddLokiFrame(this IServiceCollection services,LokiConfig config)
        {
            config.ValidateConfig();
            LokiConfig._config = config;
            
            return services;
        }
        public static T GetSettingRequired<T>(this IConfiguration config, string section)
        {
            var setting = config.GetSetting<T>(section);
            if (setting == null) throw new NullReferenceException(section + " is null");
            return setting;
        }
        public static T GetSetting<T>(this IConfiguration config, string section)
        {
            return default;
        }
    }
}