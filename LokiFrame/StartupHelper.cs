using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace LokiFrame
{
    public class StartupHelper
    {
        public static List<IComponentDefinition> GetAllComponentDefinitions()
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            List<IComponentDefinition> result = new List<IComponentDefinition>();
            foreach (var assembly in assemblies)
            {
                try
                {
                    result.AddRange(assembly.GetTypes()
                        .Where(t => t.IsSubclassOf(typeof(IComponentDefinition)) && !t.IsAbstract)
                        .Select(t => (IComponentDefinition)Activator.CreateInstance(t)));
                }
                catch (Exception e)
                {
                }
            }
            return result;
        }

        public static void InitAllMapper(IServiceCollection services)
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfiles(GetAllComponentDefinitions());
            });
            var mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }

        public static void AddAllServices(IServiceCollection services, IConfiguration configuration)
        {
            foreach (var allComponentDefinition in GetAllComponentDefinitions())
            {
                allComponentDefinition.AddServices(services,configuration);
            }
        }
    }
}