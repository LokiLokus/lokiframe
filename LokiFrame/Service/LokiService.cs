namespace LokiFrame.Service
{
    public abstract class LokiService<D>
    {
        protected readonly D _databaseCtx;

        public LokiService(D databaseCtx)
        {
            _databaseCtx = databaseCtx;
        }
    }

    public abstract class LokiService<D, M>
    {
        protected readonly D _databaseCtx;
        protected readonly M _mapper;

        public LokiService(D databaseCtx, M mapper)
        {
            _databaseCtx = databaseCtx;
            _mapper = mapper;
        }
    }
}