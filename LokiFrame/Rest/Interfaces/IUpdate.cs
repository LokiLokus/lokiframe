using System.Collections.Generic;
using System.Threading.Tasks;

namespace LokiFrame.Rest.Interfaces
{
    public interface IGet<O>
    {
        public Task<Result<IEnumerable<O>>> Get();
    }
    public interface IUpdate<I,O>
    {
        public Task<Result<O>> Update(I model,string id);
    }
    public interface ICreate<I,O>
    {
        public Task<Result<O>> Create(I model);
    }
    
    public interface IDelete
    {
        public Task<Result> Delete(string id);
    }
}