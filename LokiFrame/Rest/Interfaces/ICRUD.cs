namespace LokiFrame.Rest.Interfaces
{
  
    public interface IR<O> : IGet<O>
    {
        
    }
    public interface ICR<C,O> :IGet<O>,ICreate<C,O>
    {
        
    }

    
    public interface ICRU<C,U,O> :IGet<O>,ICreate<C,O>,IUpdate<U,O>
    {
        
    }
    
    public interface ICRUD<C,U,O> :IGet<O>,ICreate<C,O>,IUpdate<U,O>,IDelete
    {
        
    }
}