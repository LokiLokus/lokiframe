using System.Collections.Generic;
using System.Threading.Tasks;
using LokiFrame.AppConfig;
using LokiFrame.Rest.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace LokiFrame.Rest
{
    public abstract class LokiController<S> : LokiController
    {
        protected readonly S _service;

        public LokiController(S service)
        {
            _service = service;
        }
    }
    
    public abstract class LokiCR<S,C,O> : LokiController<S> where S : ICR<C,O>
    {
        public LokiCR(S service):base(service)
        {
        }

        [HttpGet("Get")]
        public abstract Task<ActionResult<IEnumerable<O>>> Get();
        [HttpPost("Create")]
        public abstract Task<ActionResult<O>> Create([FromBody]C model);
    }
    
    
    public abstract class LokiCRU<S,C,U,O> : LokiController<S> where S : ICRU<C,U,O>
    {
        public LokiCRU(S service):base(service)
        {
        }

        [HttpGet("Get")]
        public abstract Task<ActionResult<IEnumerable<O>>> Get();
        [HttpPost("Create")]
        public abstract Task<ActionResult<O>> Create([FromBody]C model);
        [HttpPut("Update/{id}")]
        public abstract Task<ActionResult<O>> Update([FromBody]U model,string id);
    }
    public abstract class LokiCRUD<S,C,U,O> : LokiController<S> where S : ICRUD<C,U,O>
    {
        public LokiCRUD(S service):base(service)
        {
        }

        [HttpGet("Get")]
        public abstract Task<ActionResult<IEnumerable<O>>> Get();
        [HttpPost("Create")]
        public abstract Task<ActionResult<O>> Create([FromBody]C model);
        [HttpPut("Update/{id}")]
        public abstract Task<ActionResult<O>> Update([FromBody]U model,string id);

        [HttpDelete("Delete/{id}")]
        public abstract Task<ActionResult<bool>> Delete([FromRoute]string id);
    }
    
}