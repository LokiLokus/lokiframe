using System;
using System.Threading.Tasks;
using LokiFrame.AppConfig;
using Microsoft.AspNetCore.Mvc;

namespace LokiFrame.Rest
{
    [Route("api/[controller]")]
    public class LokiController : ControllerBase {
        private ActionResult InternalCall<D>(Result<D> res)
        {
            if (res.Succeeded)
            {
                return Ok(res.SuccessResult);
            }
            return BadRequest(res.Errors);
        }

        private ActionResult ExceptionHandler(Exception e)
        {
            if (LokiConfig._config.PrintException)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
            if (LokiConfig._config.UseExceptionMessage)
            {
                string message = e.Message;
                if (LokiConfig._config.UseExceptionStackTrace)
                {
                    message += Environment.NewLine;
                    message += e.StackTrace;
                }
                return BadRequest(Result.Fail(LokiConfig._config.DefaultErrorCode,message).Errors);
            }
            else
            {
                return BadRequest(Result.Fail(LokiConfig._config.DefaultErrorCode,LokiConfig._config.DefaultErrorText).Errors);
            }
        }
        
        protected ActionResult CallRest<D>(Func<Result> result)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            try
            {
                Result res = result();
                return InternalCall(res);
            }
            catch (Exception e)
            {
                if (LokiConfig._config.RethrowException) throw;
                return ExceptionHandler(e);
            }
        }
        
        protected ActionResult CallRest<T>(Func<T,Result> result,T input)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            try
            {
                Result res = result(input);
                return InternalCall(res);
            }
            catch (Exception e)
            {
                if (LokiConfig._config.RethrowException) throw;
                return ExceptionHandler(e);
            }
        }
        
        protected ActionResult CallRest<T,G>(Func<T,G,Result> result,T input, G input1)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            try
            {
                Result res = result(input,input1);
                return InternalCall(res);
            }
            catch (Exception e)
            {
                if (LokiConfig._config.RethrowException) throw;
                return ExceptionHandler(e);
            }
        }
        
        protected ActionResult CallRest<T,G,N>(Func<T,G,N,Result> result,T input, G input1,N input2)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            try
            {
                Result res = result(input,input1,input2);
                return InternalCall(res);
            }
            catch (Exception e)
            {
                if (LokiConfig._config.RethrowException) throw;
                return ExceptionHandler(e);
            }
        }
        protected ActionResult CallRest<T,G,N,M>(Func<T,G,N,M,Result> result,T input, G input1,N input2,M input3)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            try
            {
                Result res = result(input,input1,input2,input3);
                return InternalCall(res);
            }
            catch (Exception e)
            {
                if (LokiConfig._config.RethrowException) throw;
                return ExceptionHandler(e);
            }
        }
        protected ActionResult CallRest<T,G,N,M,B>(Func<T,G,N,M,B,Result> result,T input, G input1,N input2,M input3,B input4)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            try
            {
                Result res = result(input,input1,input2,input3,input4);
                return InternalCall(res);
            }
            catch (Exception e)
            {
                if (LokiConfig._config.RethrowException) throw;
                return ExceptionHandler(e);
            }
        }
        
        protected async Task<ActionResult> CallRest<D>(Func<Task<Result>> result)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            try
            {
                Result res = await result();
                return InternalCall(res);
            }
            catch (Exception e)
            {
                if (LokiConfig._config.RethrowException) throw;
                return ExceptionHandler(e);
            }
        }
        
        protected async Task<ActionResult> CallRest<T>(Func<T,Task<Result>> result,T input)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            try
            {
                Result res = await result(input);
                return InternalCall(res);
            }
            catch (Exception e)
            {
                if (LokiConfig._config.RethrowException) throw;
                return ExceptionHandler(e);
            }
        }
        
        protected async Task<ActionResult> CallRest<T,G>(Func<T,G,Task<Result>> result,T input, G input1)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            try
            {
                Result res = await result(input,input1);
                return InternalCall(res);
            }
            catch (Exception e)
            {
                if (LokiConfig._config.RethrowException) throw;
                return ExceptionHandler(e);
            }
        }
        
        protected async Task<ActionResult> CallRest<T,G,N>(Func<T,G,N,Task<Result>> result,T input, G input1,N input2)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            try
            {
                Result res = await result(input,input1,input2);
                return InternalCall(res);
            }
            catch (Exception e)
            {
                if (LokiConfig._config.RethrowException) throw;
                return ExceptionHandler(e);
            }
        }
        protected async Task<ActionResult> CallRest<T,G,N,M>(Func<T,G,N,M,Task<Result>> result,T input, G input1,N input2,M input3)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            try
            {
                Result res = await result(input,input1,input2,input3);
                return InternalCall(res);
            }
            catch (Exception e)
            {
                if (LokiConfig._config.RethrowException) throw;
                return ExceptionHandler(e);
            }
        }
        protected async Task<ActionResult> CallRest<T,G,N,M,B>(Func<T,G,N,M,B,Task<Result>> result,T input, G input1,N input2,M input3,B input4)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            try
            {
                Result res = await result(input,input1,input2,input3,input4);
                return InternalCall(res);
            }
            catch (Exception e)
            {
                if (LokiConfig._config.RethrowException) throw;
                return ExceptionHandler(e);
            }
        }
        
        protected async Task<ActionResult> CallRest(Func<Task<Result>> result)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            try
            {
                Result res = await result();
                return InternalCall(res);
            }
            catch (Exception e)
            {
                if (LokiConfig._config.RethrowException) throw;
                return ExceptionHandler(e);
            }
        }
        
        protected async Task<ActionResult<D>> CallRest<D>(Func<Task<Result<D>>> result)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            try
            {
                Result<D> res = await result();
                return InternalCall(res);
            }
            catch (Exception e)
            {
                if (LokiConfig._config.RethrowException) throw;
                return ExceptionHandler(e);
            }
        }
        protected async Task<ActionResult<D>> CallRest<D,T>(Func<T,Task<Result<D>>> result,T input)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            try
            {
                Result<D> res = await result(input);
                return InternalCall(res);
            }
            catch (Exception e)
            {
                if (LokiConfig._config.RethrowException) throw;
                return ExceptionHandler(e);
            }
        }
        
        protected async Task<ActionResult<D>> CallRest<D,T,G>(Func<T,G,Task<Result<D>>> result,T input, G input1)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            try
            {
                Result<D> res = await result(input,input1);
                return InternalCall(res);
            }
            catch (Exception e)
            {
                if (LokiConfig._config.RethrowException) throw;
                return ExceptionHandler(e);
            }
        }
        
        protected async Task<ActionResult<D>> CallRest<D,T,G,N>(Func<T,G,N,Task<Result<D>>> result,T input, G input1,N input2)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            try
            {
                Result<D> res = await result(input,input1,input2);
                return InternalCall(res);
            }
            catch (Exception e)
            {
                if (LokiConfig._config.RethrowException) throw;
                return ExceptionHandler(e);
            }
        }
        protected async Task<ActionResult<D>> CallRest<D,T,G,N,M>(Func<T,G,N,M,Task<Result<D>>> result,T input, G input1,N input2,M input3)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            try
            {
                Result<D> res = await result(input,input1,input2,input3);
                return InternalCall(res);
            }
            catch (Exception e)
            {
                if (LokiConfig._config.RethrowException) throw;
                return ExceptionHandler(e);
            }
        }
        protected async Task<ActionResult<D>> CallRest<D,T,G,N,M,B>(Func<T,G,N,M,B,Task<Result<D>>> result,T input, G input1,N input2,M input3,B input4)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            try
            {
                Result<D> res = await result(input,input1,input2,input3,input4);
                return InternalCall(res);
            }
            catch (Exception e)
            {
                if (LokiConfig._config.RethrowException) throw;
                return ExceptionHandler(e);
            }
        }
        
        protected ActionResult<D> CallRest<D>(Func<Result<D>> result)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            try
            {
                Result<D> res = result();
                return InternalCall(res);
            }
            catch (Exception e)
            {
                if (LokiConfig._config.RethrowException) throw;
                return ExceptionHandler(e);
            }
        }
        
        protected ActionResult<D> CallRest<D,T>(Func<T,Result<D>> result,T input)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            try
            {
                Result<D> res = result(input);
                return InternalCall(res);
            }
            catch (Exception e)
            {
                if (LokiConfig._config.RethrowException) throw;
                return ExceptionHandler(e);
            }
        }
        
        protected ActionResult<D> CallRest<D,T,G>(Func<T,G,Result<D>> result,T input, G input1)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            try
            {
                Result<D> res = result(input,input1);
                return InternalCall(res);
            }
            catch (Exception e)
            {
                if (LokiConfig._config.RethrowException) throw;
                return ExceptionHandler(e);
            }
        }
        
        protected ActionResult<D> CallRest<D,T,G,N>(Func<T,G,N,Result<D>> result,T input, G input1,N input2)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            try
            {
                Result<D> res = result(input,input1,input2);
                return InternalCall(res);
            }
            catch (Exception e)
            {
                if (LokiConfig._config.RethrowException) throw;
                return ExceptionHandler(e);
            }
        }
        protected ActionResult<D> CallRest<D,T,G,N,M>(Func<T,G,N,M,Result<D>> result,T input, G input1,N input2,M input3)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            try
            {
                Result<D> res = result(input,input1,input2,input3);
                return InternalCall(res);
            }
            catch (Exception e)
            {
                if (LokiConfig._config.RethrowException) throw;
                return ExceptionHandler(e);
            }
        }
        protected ActionResult<D> CallRest<D,T,G,N,M,B>(Func<T,G,N,M,B,Result<D>> result,T input, G input1,N input2,M input3,B input4)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            try
            {
                Result<D> res = result(input,input1,input2,input3,input4);
                return InternalCall(res);
            }
            catch (Exception e)
            {
                if (LokiConfig._config.RethrowException) throw;
                return ExceptionHandler(e);
            }
        }
    }
}