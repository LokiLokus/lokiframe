using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace LokiFrame
{
    public abstract class IComponentDefinition: Profile
    {
        public abstract void AddServices(IServiceCollection services,IConfiguration configuration);
    }
}